package com.zuitt;

import java.util.InputMismatchException;
import java.util.Scanner;

public class StretchGoal {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);


        int num;

        long answer = 1;
        int counter;

        try {
            System.out.println("Input an integer whose factorial will be computed: ");


        } catch (InputMismatchException e) {
            System.out.println("Input is either 0 or a negative number.");

        } catch (Exception e) {
            System.out.println("Invalid Input");

        } finally {
            num = in.nextInt();
            for(counter = 1; counter <= num; counter++){
               answer = answer * counter;
            }
            System.out.println("The factorial of " + num + " is: " + answer);
        }

    }
}
