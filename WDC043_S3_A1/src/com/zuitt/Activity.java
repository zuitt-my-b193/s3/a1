package com.zuitt;

import java.util.Scanner;

public class Activity {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);

        int num;
        System.out.println("Input an integer whose factorial will be computed: ");
        num = in.nextInt();
        in.close();

        long answer = 1;
        int counter = 1;

        //WHILE LOOP
        while(counter<=num){
            answer = answer * counter;
            counter++;
        }
        System.out.println("The factorial of " +num+ " is: " + answer);
    }
}
